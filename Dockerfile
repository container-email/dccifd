ARG DVER=latest
FROM docker.io/alpine:$DVER
LABEL maintainer="Duncan Bellamy <dunk@denkimushi.com>"
ARG APKVER

RUN apk update \
&& apk upgrade --available --no-cache \
&& apk add --no-cache --upgrade dcc-dccifd$APKVER \
&& mkdir /var/dcc/sock && chown dcc:dcc /var/dcc/sock

WORKDIR /usr/local/bin
COPY --chmod=755 container-scripts/set-timezone.sh entrypoint.sh ./
WORKDIR /var/dcc

CMD [ "entrypoint.sh" ]
VOLUME /var/dcc/log
EXPOSE 10045
